const {Note} = require('../models/noteModel');

const getNotes = async (req, res) => {
    const notes = await Note.find({userId: req.user.id});

    res.status(200).json({
        notes: notes
    });
};

const createNote = async (req, res) => {
    const note = new Note({
        userId: req.user.id,
        text: req.body.text
    });

    await note.save();

    res.status(200).json({
        message: 'Success'
    });
};

const getNote = async (req, res) => {
    const id = req.params.id;
    const note = await Note.findOne({_id: id});

    res.status(200).json({
        note: {
            _id: note._id,
            userId: note.userId,
            completed: note.completed,
            text: note.text,
            createdDate: note.createdDate
        }
    });

};

const editNote = async (req, res) => {
    const id = req.params.id;
    const note = await Note.findOne({_id: id});

    await note.update({text: req.body.text || ''});

    res.status(200).json({
        message: 'Success'
    });

};

const checkUncheckNote = async (req, res) => {
    const id = req.params.id;
    const note = await Note.findOne({_id: id});

    note.completed ? await note.update({completed: false}) : await note.update({completed: true});
    
    res.status(200).json({
        message: 'Success'
    });

};

const deleteNote = async (req, res) => {
    const id = req.params.id;
    const note = await Note.findOne({_id: id});

    await note.remove();
    
    res.status(200).json({
        message: 'Success'
    });

};

module.exports = {getNotes, createNote, getNote, editNote, checkUncheckNote, deleteNote}