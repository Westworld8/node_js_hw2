const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const {User} = require('../models/userModel');
const {JWT_SECRET} = require('../config');

const registerUser = async (req, res) => {
    const {username, password} = req.body;
    
    const user = new User({
        username: username,
        password: await bcrypt.hash(password, 10)
    });

    await user.save();

    res.status(200).json({
        message: 'Success'
    });
}

const loginUser = async (req, res) => {
    const {username, password} = req.body;
    const user = await User.findOne({username});

    const token = jwt.sign({username: user.username, id: user._id}, JWT_SECRET);

    res.status(200).json({
        message: 'Success',
        jwt_token: token
    });
}

module.exports = {registerUser, loginUser};