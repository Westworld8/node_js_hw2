const bcrypt = require('bcrypt');

const {User} = require('../models/userModel');

const getUserInfo = async (req, res) => {
    const id = req.user.id;
    const user = await User.findOne({_id: id});

    res.status(200).json({
        message: 'Success',
        user: {
            _id: user._id,
            username: user.username,
            createdDate: user.createdDate
        }
    });
};

const editUserInfo = async (req, res) => {
    const id = req.user.id;
    const user = await User.findOne({_id: id});

    const {oldPassword, newPassword} = req.body;

    await user.update({password: await bcrypt.hash(newPassword, 10)});

    res.status(200).json({
        message: 'Success'
    });
};

const deleteUser = async (req, res) => {
    const id = req.user.id;
    const user = await User.findOne({_id: id});

    await user.remove();

    res.status(200).json({
        message: 'Success'
    });
};

module.exports = {getUserInfo, editUserInfo, deleteUser};