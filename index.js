require('dotenv').config();
const mongoose = require('mongoose');
const express = require('express');
const morgan = require('morgan');
const app = express();

const port = process.env.PORT || 8080;

const authRouter = require('./routers/authRouter');
const usersRouter = require('./routers/usersRouter');
const notesRouter = require('./routers/notesRouter');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', usersRouter);
app.use('/api/notes', notesRouter);

app.use((err, req, res, next) => {
  res.status(500).json({
    message: err.message,
  });
});

const serverStart = async () => {
  await mongoose.connect('mongodb+srv://TestUser:TestPass1234@cluster0.ablfs.mongodb.net/Notes_DB?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  app.listen(port, () => {
    console.log('Server is running');
  });
};

serverStart();
