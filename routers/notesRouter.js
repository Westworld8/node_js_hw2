const express = require('express');
const router = express.Router();

const {asyncWrapper} = require('./helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {validateAccess} = require('./middlewares/notesMiddleware');
const {getNotes, createNote, getNote, editNote, checkUncheckNote, deleteNote} = require('../controllers/notesController');

router.get('/', authMiddleware, asyncWrapper(getNotes));
router.post('/', authMiddleware, asyncWrapper(createNote));
router.get('/:id', authMiddleware, asyncWrapper(validateAccess), asyncWrapper(getNote));
router.put('/:id', authMiddleware, asyncWrapper(validateAccess), asyncWrapper(editNote));
router.patch('/:id', authMiddleware, asyncWrapper(validateAccess), asyncWrapper(checkUncheckNote));
router.delete('/:id', authMiddleware, asyncWrapper(validateAccess), asyncWrapper(deleteNote));

module.exports = router;