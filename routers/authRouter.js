const express = require('express');
const router = express.Router();

const {asyncWrapper} = require('./helpers');
const {validateRedistration, validateLogin} = require('./middlewares/validationMiddleware');
const {validatePassword} = require('./middlewares/authMiddleware');
const {registerUser, loginUser} = require('../controllers/authController');

router.post('/register', asyncWrapper(validateRedistration), asyncWrapper(registerUser));
router.post('/login', asyncWrapper(validateLogin), asyncWrapper(validatePassword), asyncWrapper(loginUser));

module.exports = router;