const {Note} = require('../../models/noteModel');

const validateAccess = async (req, res, next) => {
    const id = req.params.id;

    const note = await Note.findOne({_id: id});
    if(!note) {
        return res.status(400).json({
            message: 'Note not found'
        });
    }

    const userId = note.userId;
    if(req.user.id !== userId) {
        return res.status(401).json({
            message: 'Not authorized'
        });
    }

    next();
};

module.exports = {validateAccess};