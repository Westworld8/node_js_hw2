const bcrypt = require('bcrypt');

const {User} = require('../../models/userModel');

const validateExist = async (req, res, next) => {
    const id = req.user.id;
    const user = await User.findOne({_id: id});

    if(!user) {
        return res.status(400).json({
            message: 'User not found'
        });
    }

    next();
};

const validateChangingPassword = async (req, res, next) => {
    const id = req.user.id;
    const user = await User.findOne({_id: id});

    const {oldPassword, newPassword} = req.body;

    if(!(await bcrypt.compare(oldPassword, user.password))) {
        return res.status(400).json({
            message: `Incorrect old password`
        });
    }

    if(oldPassword === newPassword) {
        return res.status(400).json({
            message: `Old password is equal to new password`
        });
    }

    next();
};

module.exports = {validateExist, validateChangingPassword};