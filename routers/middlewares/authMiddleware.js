const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const {JWT_SECRET} = require('../../config');
const {User} = require('../../models/userModel');

const authMiddleware = (req, res, next) => {
    const [tokenType, token] = req.headers['authorization'].split(' ');

    if(!token) {
        return res.status(401).json({
            message: 'Not authorized'
        });
    }

    if(tokenType !== 'JWT') {
        return res.status(401).json({
            message: 'Required JWT token type'
        });
    }

    req.user = jwt.verify(token, JWT_SECRET);
    next();
};

const validatePassword = async (req, res, next) => {
    const {username, password} = req.body;
    const user = await User.findOne({username});

    if(!user || !(await bcrypt.compare(password, user.password))) {
        return res.status(400).json({
            message: `Wrong username or password`
        });
    }

    next();
};

module.exports = {authMiddleware, validatePassword};
