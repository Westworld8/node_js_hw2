const express = require('express');
const router = express.Router();

const {asyncWrapper} = require('./helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {validateExist, validateChangingPassword} = require('./middlewares/usersMiddleware');
const {getUserInfo, editUserInfo, deleteUser} = require('../controllers/usersController');

router.get('/', authMiddleware, asyncWrapper(validateExist), asyncWrapper(getUserInfo));
router.patch('/', authMiddleware, asyncWrapper(validateExist), asyncWrapper(validateChangingPassword), asyncWrapper(editUserInfo));
router.delete('/', authMiddleware, asyncWrapper(validateExist), asyncWrapper(deleteUser));

module.exports = router;